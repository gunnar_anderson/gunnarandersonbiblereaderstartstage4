package bibleReader;

import java.io.File;

import javax.swing.*;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.*;

import bibleReader.model.ArrayListBible;
import bibleReader.model.Bible;
import bibleReader.model.BibleReaderModel;
import bibleReader.model.TreeMapBible;
import bibleReader.model.VerseList;

/**
 * The main class for the Bible Reader Application.
 * 
 * @author cusack
 */
public class BibleReaderApp extends JFrame {
	// Change these to suit your needs.
	public static final int width = 600;
	public static final int height = 600;
	public String inputText;
	public JFrame frame;
	public JButton search, passage;
	public JTextField input;
	public JPanel searchPanel;
	private JMenuBar menuBar;
	private JMenu file, help;
	private JMenuItem exit, about;

	public static void main(String[] args) {
		new BibleReaderApp();
	}

	// Fields
	private BibleReaderModel model;
	private ResultView resultView;

	// TODO add more fields as necessary

	/**
	 * Default constructor. We may want to replace this with a different one.
	 */
	public BibleReaderApp() {
		// There is no guarantee that this complete/correct, so take a close
		// look to make sure you understand what this code is doing in case
		// you need to modify or add to it.
		model = new BibleReaderModel(); // For now call the default constructor.
										// This might change.
		File kjvFile = new File("kjv.atv");
		VerseList verses = BibleIO.readBible(kjvFile);

		Bible kjv = new ArrayListBible(verses);

		model.addBible(kjv);

		resultView = new ResultView(model);

		setupGUI();
		frame.pack();
		frame.setSize(width, height);

		// So the application exits when you click the "x".
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}

	/**
	 * Set up the main GUI. Make sure you don't forget to put resultView somewhere!
	 */
	private void setupGUI() {
		// TODO textfield and button that allow a word search to be performed:
		// Stage 5
		// TODO Display search results (in the ResulteView): Stage 5

		// The stage numbers below may change, so make sure to pay attention to
		// what the assignment says.
		// TODO Add passage lookup: Stage ?
		// TODO Add 2nd version on display: Stage ?
		// TODO Limit the displayed search results to 20 at a time: Stage ?
		// TODO Add 3rd versions on display: Stage ?
		// TODO Format results better: Stage ?
		// TODO Display cross references for third version: Stage ?
		// TODO Save/load search results: Stage ?
		// add an input JTextField

		// Create the frame
		frame = new JFrame("Bible Reader App");
		// create the components to be added to the container
		// Add a JMenuBar with file and help tabs. The file tab
    	// has an exit tab which exits the application, the help 
    	// tab shows info about who wrote the application.
        menuBar = new JMenuBar();
        file = new JMenu("File");
        help = new JMenu("Help");
        exit = new JMenuItem("Exit");
        about = new JMenuItem("About");
        exit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        about.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, "This application was created by Chuck Cusack, Quentin Couvelaire, and Gunnar Anderson, it was developed using the eclipse IDE.");
            }
        });
        file.add(exit);
        help.add(about);
        menuBar.add(file);
        menuBar.add(help);
        frame.add(menuBar);
        frame.setJMenuBar(menuBar);
        
		input = new JTextField(20);
		input.addKeyListener(new KeyListener() {
			public void keyTyped(KeyEvent e) {
				inputText = input.getText();
			}

			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub

			}

			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub

			}
		});

		// add a search button that searches based on the inputText
		search = new JButton("Search");
		search.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultView.displayResults(inputText);
			}
		});
		
		// add a passage button to allow for passage lookup
		passage = new JButton("Passage");
		passage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultView.displayResults(inputText);
			}
		});

		// create the container to add components too
		Container contents = frame.getContentPane();
		contents.setLayout(new BorderLayout());
		// create a JPanel to add the search bar and button to
		searchPanel = new JPanel();
		searchPanel.setLayout(new FlowLayout());
		searchPanel.add(input);
		searchPanel.add(search);
		// add the components of the frame
		contents.add(searchPanel, BorderLayout.NORTH);
		contents.add(resultView, BorderLayout.CENTER);
	}

}













