package bibleReader.model;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * The model of the Bible Reader. It stores the Bibles and has methods for
 * searching for verses based on words or references.
 * 
 * @author cusack
 */
public class BibleReaderModel implements MultiBibleModel {

	// ---------------------------------------------------------------------------
	// TODO Add more fields here: Stage 5
	// You need to store several Bible objects.
	// You may need to store other data as well.

	private ArrayList<Bible> bibles;

	/**
	 * Default constructor. You probably need to instantiate objects and do other
	 * assorted things to set up the model.
	 */
	public BibleReaderModel() {
		bibles = new ArrayList<Bible>();
	}

	@Override
	public String[] getVersions() {
		String[] returnArray = new String[bibles.size()];

		for (int i = 0; i < bibles.size(); i++) {
			returnArray[i] = bibles.get(i).getVersion();
		}

		Arrays.sort(returnArray);
		return returnArray;
	}

	@Override
	public int getNumberOfVersions() {
		return bibles.size();
	}

	@Override
	public void addBible(Bible bible) {
		bibles.add(bible);
	}

	@Override
	public Bible getBible(String version) {
		for (Bible b : bibles) {
			if (b.getVersion().equals(version)) {
				return b;
			}
		}
		return null;
	}

	@Override
	public ArrayList<Reference> getReferencesContaining(String words) {
		ArrayList<Reference> returnList = new ArrayList<Reference>();

		for (Bible b : bibles) {
			ArrayList<Reference> references = b.getReferencesContaining(words);
			for (Reference r : references) {
				returnList.add(r);
			}
		}

		return returnList;
	}

	@Override
	public VerseList getVerses(String version, ArrayList<Reference> references) {
		VerseList verses = new VerseList(version, "");
		for (Bible b : bibles) {
			if (b.getVersion().equals(version)) {
				for (Reference r : references) {
					if (b.isValid(r)) {
						verses.add(b.getVerse(r));
					}
				}
			}
		}
		return verses;
	}
	// ---------------------------------------------------------------------

	@Override
	/**
	 * @param version   The version of the Bible to look in.
	 * @param reference The Reference we want the text for from the given version.
	 * @return The text for the given Reference in the given Bible, or the empty
	 *         string if the Reference is not found.
	 */
	public String getText(String version, Reference reference) {
		String returnString = "";
		for (Bible b : bibles) {
			if (b.getVersion().equals(version)) {
				returnString = b.getVerseText(reference);
			}
		}
		return returnString;
	}

	@Override
	/**
	 * Returns a list of the references for the given passage, in order. If not all
	 * version contain all of the references, return all of the references in the
	 * passage that are in any of the versions. In other words, combine the lists
	 * from all of the versions, keeping any reference that occurs in any of the
	 * versions, and only listing each reference once no matter how many versions it
	 * appears in. Otherwise, returns an empty ArrayList if the passage is invalid.
	 * 
	 * @param reference A string representation of the reference (e.g. "Genesis
	 *                  1:2-3:4")
	 * @return A ArrayList<Reference> containing all of the verses for the given
	 *         passage, in order.
	 */
	public ArrayList<Reference> getReferencesForPassage(String reference) {
		ArrayList<Reference> returnList = new ArrayList<>();
		String[] parts = reference.split(" ");// should have 2 parts, 1 being the Book, and the other being the mess
												// with chapters and verses
		String book = parts[0]; // the book string
		BookOfBible b = BookOfBible.getBookOfBible(book);
		String cv = parts[1]; // the chapter(s) and verse(s)
		String[] subparts = cv.split("-"); // splits at dash to either yield two chapters and verses or just one
		String cv1 = subparts[0]; // first chapter and verse
		String cv2 = subparts[1]; // second chapter and verse
		String[] chapterVerse1 = cv1.split(":");
		String[] chapterVerse2 = cv2.split(":");
		int chapter1 = Integer.parseInt(chapterVerse1[0]);
		int verse1 = Integer.parseInt(chapterVerse1[1]);
		int chapter2 = Integer.parseInt(chapterVerse2[0]);
		int verse2 = Integer.parseInt(chapterVerse2[1]);
		for (Bible bib : bibles) {
			returnList = bib.getReferencesForPassage(b, chapter1, verse1, chapter2, verse2);
		}
		return returnList;
	}

	// -----------------------------------------------------------------------------
	// The next set of methods are for use by the getReferencesForPassage method
	// above.
	// After it parses the input string it will call one of these.
	//
	// These methods should be somewhat easy to implement. They are kind of
	// delegate
	// methods in that they call a method on the Bible class to do most of the
	// work.
	// However, they need to do so for every version of the Bible stored in the
	// model.
	// and combine the results.
	//
	// Once you implement one of these, the rest of them should be fairly
	// straightforward.
	// Think before you code, get one to work, and then implement the rest based
	// on
	// that one.
	// -----------------------------------------------------------------------------

	@Override
	/**
	 * Returns a list containing the single reference "book chapter:verse" (e.g.
	 * John 3:16) if the reference occurs in any of the versions, or an empty list
	 * if it does not occur in any of the versions.
	 * 
	 * @param book    The book of the bible
	 * @param chapter the chapter
	 * @param verse   the verse
	 * @return a list containing the single reference requested, or an empty list if
	 *         the reference does not appear in any of the versions.
	 */
	public ArrayList<Reference> getVerseReferences(BookOfBible book, int chapter, int verse) {
		ArrayList<Reference> returnList = new ArrayList<>();
		Reference r = new Reference(book, chapter, verse);
		for (Bible b : bibles) {
			if (b.isValid(r)) {
				returnList.add(r);
			}
		}
		return returnList;
	}

	@Override

	public ArrayList<Reference> getPassageReferences(Reference startVerse, Reference endVerse) {
		ArrayList<Reference> returnList = new ArrayList<>();
		for (Bible b : bibles) {
			returnList = b.getReferencesInclusive(startVerse, endVerse);
		}
		return returnList;
	}

	@Override
	public ArrayList<Reference> getBookReferences(BookOfBible book) {
		ArrayList<Reference> returnList = new ArrayList<>();
		for (Bible b : bibles) {
			returnList = b.getReferencesForBook(book);
		}
		return returnList;
	}

	@Override
	public ArrayList<Reference> getChapterReferences(BookOfBible book, int chapter) {
		ArrayList<Reference> returnList = new ArrayList<>();
		for (Bible b : bibles) {
			returnList = b.getReferencesForChapter(book, chapter);
		}
		return returnList;
	}

	@Override
	public ArrayList<Reference> getChapterReferences(BookOfBible book, int chapter1, int chapter2) {
		ArrayList<Reference> returnList = new ArrayList<>();
		for (Bible b : bibles) {
			returnList = b.getReferencesForChapters(book, chapter1, chapter2);
		}
		return returnList;
	}

	@Override
	public ArrayList<Reference> getPassageReferences(BookOfBible book, int chapter, int verse1, int verse2) {
		ArrayList<Reference> returnList = new ArrayList<>();
		for (Bible b : bibles) {
			returnList = b.getReferencesForPassage(book, chapter, verse1, verse2);
		}
		return returnList;
	}

	@Override
	public ArrayList<Reference> getPassageReferences(BookOfBible book, int chapter1, int verse1, int chapter2,
			int verse2) {
		ArrayList<Reference> returnList = new ArrayList<>();
		for (Bible b : bibles) {
			returnList = b.getReferencesForPassage(book, chapter1, verse1, chapter2, verse2);
		}
		return returnList;
	}

	// ------------------------------------------------------------------
	// These are the better searching methods.
	//
	@Override
	public ArrayList<Reference> getReferencesContainingWord(String word) {
		// TODO Implement me: Stage 12
		return null;
	}

	@Override
	public ArrayList<Reference> getReferencesContainingAllWords(String words) {
		// TODO Implement me: Stage 12
		return null;
	}

	@Override
	public ArrayList<Reference> getReferencesContainingAllWordsAndPhrases(String words) {
		// TODO Implement me: Stage 12
		return null;
	}
}



