
package bibleReader.model;

import java.util.ArrayList;

/**
 * A class that stores a version of the Bible.
 * 
 * @author Chuck Cusack
 */
public class ArrayListBible implements Bible {
	// The Fields
	// private String version;
	// private String title;
	// I modified this to use VerseList so that I can use
	// indexOfBinarySearch method that I recently added.
	// CAC, 3/12/15
	private VerseList theVerses;

	/**
	 * Create a new Bible with the given verses.
	 * 
	 * @param verses
	 *            A VerseList with all of the verses for this Bible.
	 */
	public ArrayListBible(VerseList verses) {
		theVerses = new VerseList(verses);
		theVerses.add(new Verse(BookOfBible.Dummy, 1, 1, ""));
	}

	@Override
	public int getNumberOfVerses() {
		return theVerses.size() - 1;
	}

	@Override
	public VerseList getAllVerses() {
		ArrayList<Verse> verses = theVerses.copyVerses();
		verses.remove(new Verse(BookOfBible.Dummy, 1, 1, ""));
		return new VerseList(getVersion(), getTitle(), verses);
	}

	@Override
	public String getVersion() {
		return theVerses.getVersion();
	}

	@Override
	public String getTitle() {
		return theVerses.getDescription();
	}

	@Override
	public boolean isValid(Reference ref) {
		if (ref.getBookOfBible() == BookOfBible.Dummy) {
			return false;
		}
		for (Verse v : theVerses) {
			if (v.getReference().equals(ref)) {
				return true;
			}
		}
		return false;
	}

	//---------------------------------------------------
	// A few helper methods.
	/*
	 * Are the verses in order and are they both valid?
	 */
	private boolean isValidRange(Reference firstRef, Reference lastRef) {
		return isValid(firstRef) && isValid(lastRef) && firstRef.compareTo(lastRef) <= 0;
	}

	private int indexOfVerseWithReference(Reference ref) {
		int i = 0;
		while (i < theVerses.size() && !theVerses.get(i).getReference().equals(ref)) {
			i++;
		}
		if (i < theVerses.size()) {
			return i;
		} else {
			return -1;
		}
	}
	//---------------------------------------------------
	
	@Override
	public String getVerseText(Reference r) {
		for (Verse v : theVerses) {
			if (v.getReference().equals(r)) {
				return v.getText();
			}
		}
		return null;
	}

	@Override
	public Verse getVerse(Reference r) {
		for (Verse v : theVerses) {
			if (v.getReference().equals(r)) {
				return v;
			}
		}
		return null;
	}

	@Override
	public Verse getVerse(BookOfBible book, int chapter, int verse) {
		Reference ref = new Reference(book, chapter, verse);
		return getVerse(ref);
	}

	@Override
	public VerseList getVerses(ArrayList<Reference> references) {
		// No search term or anything given. Empty string?
		VerseList results = new VerseList(getVersion(), "Arbitrary list of Verses");
		for (Reference ref : references) {
			results.add(getVerse(ref));
		}
		return results;
	}

	@Override
	public VerseList getVersesContaining(String phrase) {
		VerseList results = new VerseList(getVersion(), phrase);
		// The empty string will match every verse. Not good.
		if (phrase.length() == 0) {
			return results;
		}
		String lowerCasePhrase = phrase.toLowerCase();
		for (Verse v : theVerses) {
			if (v.getText().toLowerCase().contains(lowerCasePhrase)) {
				results.add(v);
			}
		}
		return results;
	}

	@Override
	public ArrayList<Reference> getReferencesContaining(String phrase) {
		ArrayList<Reference> results = new ArrayList<Reference>();
		// The empty string will match every verse. Not good.
		if (phrase.length() == 0) {
			return results;
		}
		String lowerCasePhrase = phrase.toLowerCase();
		for (Verse v : theVerses) {
			if (v.getText().toLowerCase().contains(lowerCasePhrase)) {
				results.add(v.getReference());
			}
		}
		return results;
	}

	@Override
	public int getLastVerseNumber(BookOfBible book, int chapter) {
		//ArrayList<Verse> verses = getChapter(book, chapter);
		//return verses.get(verses.size() - 1).getReference().getVerse();
		ArrayList<Reference> refs = getReferencesForChapter(book, chapter);
		return refs.get(refs.size() - 1).getVerse();
	}

	@Override
	public int getLastChapterNumber(BookOfBible book) {
		return -1;
	}

	@Override
	public ArrayList<Reference> getReferencesInclusive(Reference firstVerse, Reference lastVerse) {
		ArrayList<Reference> results = new ArrayList<Reference>();
		if (!isValidRange(firstVerse, lastVerse)) {
			return results;
		}
		int i = indexOfVerseWithReference(firstVerse);
		while (i < theVerses.size() && theVerses.get(i).getReference().compareTo(lastVerse) <= 0) {
			results.add(theVerses.get(i).getReference());
			i++;
		}
		return results;
	}

	@Override
	public ArrayList<Reference> getReferencesExclusive(Reference firstVerse, Reference lastVerse) {
		ArrayList<Reference> results = new ArrayList<Reference>();
		if (!isValid(firstVerse) || firstVerse.compareTo(lastVerse) > 0) {
			return results;
		}
		int i = indexOfVerseWithReference(firstVerse);
		while (i < theVerses.size() && theVerses.get(i).getReference().compareTo(lastVerse) < 0) {
			results.add(theVerses.get(i).getReference());
			i++;
		}
		return results;
	}

	@Override
	public ArrayList<Reference> getReferencesForBook(BookOfBible book) {
		if (book != null) {
			// Get the first verse of the book
			Reference firstVerse = new Reference(book, 1, 1);
			// Get the first verse of the next book.
			BookOfBible next = BookOfBible.nextBook(book);
			Reference lastVerse = new Reference(next, 1, 1);
			// now get the verses in between, excluding the last one.
			return getReferencesExclusive(firstVerse, lastVerse);
		} else {
			return new ArrayList<Reference>();
		}
	}

	@Override
	public ArrayList<Reference> getReferencesForChapter(BookOfBible book, int chapter) {
		Reference ref1 = new Reference(book, chapter, 1);
		// Making up a reference that would occur after the last one I want.
		Reference ref2 = new Reference(book, chapter + 1, 1);
		return getReferencesExclusive(ref1, ref2);
	}

	@Override
	public ArrayList<Reference> getReferencesForChapters(BookOfBible book, int chapter1, int chapter2) {
		Reference ref1 = new Reference(book, chapter1, 1);
		// Making up a reference that would occur after the last one I want.
		Reference ref2 = new Reference(book, chapter2 + 1, 1);
		return getReferencesExclusive(ref1, ref2);
	}

	@Override
	public ArrayList<Reference> getReferencesForPassage(BookOfBible book, int chapter, int verse1, int verse2) {
		// Get the first verse of the book
		Reference ref1 = new Reference(book, chapter, verse1);
		Reference ref2 = new Reference(book, chapter, verse2);
		// now get the verses in between
		return getReferencesInclusive(ref1, ref2);
	}

	@Override
	public ArrayList<Reference> getReferencesForPassage(BookOfBible book, int chapter1, int verse1, int chapter2, int verse2) {
		// Get the first verse of the book
		Reference ref1 = new Reference(book, chapter1, verse1);
		Reference ref2 = new Reference(book, chapter2, verse2);
		// now get the verses in between
		return getReferencesInclusive(ref1, ref2);
	}

	@Override
	public VerseList getVersesInclusive(Reference firstVerse, Reference lastVerse) {
		VerseList results = new VerseList(getVersion(), firstVerse+"-"+lastVerse);
		if (!isValidRange(firstVerse, lastVerse)) {
			return results;
		}
		int i = indexOfVerseWithReference(firstVerse);
		while (i < theVerses.size() && theVerses.get(i).getReference().compareTo(lastVerse) <= 0) {
			results.add(theVerses.get(i));
			i++;
		}
		return results;
	}

	@Override
	public VerseList getVersesExclusive(Reference firstVerse, Reference lastVerse) {
		VerseList results = new VerseList(getVersion(), firstVerse+"-"+lastVerse+", exclusive");
		if (!isValid(firstVerse) || firstVerse.compareTo(lastVerse) > 0) {
			return results;
		}
		int i = indexOfVerseWithReference(firstVerse);
		while (i < theVerses.size() && theVerses.get(i).getReference().compareTo(lastVerse) < 0) {
			results.add(theVerses.get(i));
			i++;
		}
		return results;
	}

	@Override
	public VerseList getBook(BookOfBible book) {
		if (book != null) {
			// Get the first verse of the book
			Reference firstVerse = new Reference(book, 1, 1);
			// Get the first verse of the next book.
			BookOfBible next = BookOfBible.nextBook(book);
			Reference lastVerse = new Reference(next, 1, 1);
			// now get the verses in between, excluding the last one.
			return getVersesExclusive(firstVerse, lastVerse);
		} else {
			return new VerseList(getVersion(), "invalid book");
		}
	}

	@Override
	public VerseList getChapter(BookOfBible book, int chapter) {
		Reference ref1 = new Reference(book, chapter, 1);
		// Making up a reference that would occur after the last one I want.
		Reference ref2 = new Reference(book, chapter + 1, 1);
		return getVersesExclusive(ref1, ref2);
	}

	@Override
	public VerseList getChapters(BookOfBible book, int chapter1, int chapter2) {
		Reference ref1 = new Reference(book, chapter1, 1);
		// Making up a reference that would occur after the last one I want.
		Reference ref2 = new Reference(book, chapter2 + 1, 1);
		return getVersesExclusive(ref1, ref2);
	}

	@Override
	public VerseList getPassage(BookOfBible book, int chapter, int verse1, int verse2) {
		// Get the first verse of the book
		Reference ref1 = new Reference(book, chapter, verse1);
		Reference ref2 = new Reference(book, chapter, verse2);
		// now get the verses in between
		return getVersesInclusive(ref1, ref2);
	}

	@Override
	public VerseList getPassage(BookOfBible book, int chapter1, int verse1, int chapter2, int verse2) {
		// Get the first verse of the book
		Reference ref1 = new Reference(book, chapter1, verse1);
		Reference ref2 = new Reference(book, chapter2, verse2);
		// now get the verses in between
		return getVersesInclusive(ref1, ref2);
	}
}




