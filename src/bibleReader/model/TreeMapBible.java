package bibleReader.model;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * A class that stores a version of the Bible.
 * 
 * @author Chuck Cusack (Provided the interface)
 * @author Gunnar Anderson (provided the implementation)
 */
public class TreeMapBible implements Bible {

	// The Fields
	private String version;
	private TreeMap<Reference, String> theVerses;

	// Or replace the above with:
	// private TreeMap<Reference, Verse> theVerses;
	// Add more fields as necessary.

	/**
	 * Create a new Bible with the given verses.
	 * 
	 * @param version the version of the Bible (e.g. ESV, KJV, ASV, NIV).
	 * @param verses  All of the verses of this version of the Bible.
	 */
	public TreeMapBible(VerseList verses, String version) {
		theVerses = new TreeMap<Reference, String>();
		ArrayList<Verse> v = verses.copyVerses();
		for (Verse verse : v) {
			theVerses.put(verse.getReference(), verse.getText());
		}
	}
	public TreeMapBible(VerseList verses) {
		theVerses = new TreeMap<Reference, String>();
		ArrayList<Verse> v = verses.copyVerses();
		for (Verse verse : v) {
			theVerses.put(verse.getReference(), verse.getText());
		}
	}

	// Override
	public int getNumberOfVerses() {
		return theVerses.size();
	}

	// Override
	public VerseList getAllVerses() {
		ArrayList<Verse> returnList = new ArrayList<>();
		Set<Map.Entry<Reference, String>> mySet = theVerses.entrySet();
		for (Map.Entry<Reference, String> element : mySet) {
			Verse aVerse = new Verse(element.getKey(), element.getValue());
			returnList.add(aVerse);
		}
		VerseList vl = new VerseList(version, "description", returnList);
		return vl;

	}

	// Override
	public String getVersion() {
		return version;
	}

	// Override
	public String getTitle() {
		return getAllVerses().getDescription();
	}

	// Override
	public boolean isValid(Reference ref) {
		if (theVerses.containsKey(ref)) {
			return true;
		}
		return false;
	}

	// Override
	public String getVerseText(Reference r) {
		if (isValid(r)) {
			String verse = theVerses.get(r);
			return verse;

		}
		return null;
	}

	// Override
	public Verse getVerse(Reference r) {
		if (isValid(r)) {
			Set<Map.Entry<Reference, String>> mySet = theVerses.entrySet();
			for (Map.Entry<Reference, String> element : mySet) {
				Verse aVerse = new Verse(element.getKey(), element.getValue());
				if (aVerse.getReference().equals(r)) {
					return aVerse;
				}

			}

		}
		return null;

	}

	// Override
	public Verse getVerse(BookOfBible book, int chapter, int verse) {
		Reference r = new Reference(book, chapter, verse);
		return getVerse(r);

	}

	// ---------------------------------------------------------------------------------------------
	// The following part of this class should be implemented for stage 4.
	//
	// For Stage 11 the first two methods below will be implemented as specified in
	// the comments.
	// Do not over think these methods. All three should be pretty straightforward
	// to implement.
	// For Stage 8 (give or take a 1 or 2) you will re-implement them so they work
	// better.
	// At that stage you will create another class to facilitate searching and use
	// it here.
	// (Essentially these two methods will be delegate methods.)
	// ---------------------------------------------------------------------------------------------

	// Override
	public VerseList getVersesContaining(String phrase) {
		VerseList results = new VerseList(getVersion(), phrase);
		if (phrase.length() == 0) {
			return results;
		}
		Set<Map.Entry<Reference, String>> mySet = theVerses.entrySet();
		for (Map.Entry<Reference, String> element : mySet) {
			Verse aVerse = new Verse(element.getKey(), element.getValue());
			if (aVerse.getText().contains(phrase.toLowerCase())) {
				results.add(aVerse);
			}
		}
		return results;
	}

	// Override
	public ArrayList<Reference> getReferencesContaining(String phrase) {
		ArrayList<Reference> returnList = new ArrayList<>();
		if (phrase.length() == 0) {
			return returnList;
		}
		Set<Map.Entry<Reference, String>> mySet = theVerses.entrySet();
		for (Map.Entry<Reference, String> element : mySet) {
			Verse aVerse = new Verse(element.getKey(), element.getValue());
			if (aVerse.getText().contains(phrase.toLowerCase())) {
				returnList.add(aVerse.getReference());
			}
			return returnList;
		}
		return null;
	}

	// Override
	public VerseList getVerses(ArrayList<Reference> references) {
		ArrayList<Verse> verses = new ArrayList<>();
		Set<Map.Entry<Reference, String>> mySet = theVerses.entrySet();
		for (Map.Entry<Reference, String> element : mySet) {
			Verse aVerse = new Verse(element.getKey(), element.getValue());
			for (Reference ref : references) {
				if (aVerse.getReference().equals(ref)) {
					verses.add(aVerse);
				}
			}

		}
		VerseList vl = new VerseList(version, "description", verses);
		return vl;
	}

	// ---------------------------------------------------------------------------------------------
	// The following part of this class should be implemented for Stage 11.
	//
	// HINT: Do not reinvent the wheel. Some of these methods can be implemented by
	// looking up
	// one or two things and calling another method to do the bulk of the work.
	// ---------------------------------------------------------------------------------------------

	// Override
	public int getLastVerseNumber(BookOfBible book, int chapter) {
		// TODO Implement me: Stage 11
				return -1;
	}

	// Override
	public int getLastChapterNumber(BookOfBible book) {
		// TODO Implement me: Stage 11
		return -1;
	}

	// Override
	public ArrayList<Reference> getReferencesInclusive(Reference firstVerse, Reference lastVerse) {
		ArrayList<Reference> references = new ArrayList<>();
		SortedMap<Reference, String> s = theVerses.subMap(firstVerse, true, lastVerse, true);
		Set<Map.Entry<Reference, String>> mySet = s.entrySet();
		for (Map.Entry<Reference, String> element : mySet) {
			Verse aVerse = new Verse(element.getKey(), element.getValue());
			references.add(aVerse.getReference());
		}
		return references;
	}

	// Override
	public ArrayList<Reference> getReferencesExclusive(Reference firstVerse, Reference lastVerse) {
		ArrayList<Reference> references = new ArrayList<>();
		SortedMap<Reference, String> s = theVerses.subMap(firstVerse, lastVerse);
		Set<Map.Entry<Reference, String>> mySet = s.entrySet();
		for (Map.Entry<Reference, String> element : mySet) {
			Verse aVerse = new Verse(element.getKey(), element.getValue());
			references.add(aVerse.getReference());
		}
		return references;
	}

	// Override
	public ArrayList<Reference> getReferencesForBook(BookOfBible book) {
		if (book != null) {
			Reference firstVerse = new Reference(book, 1, 1);
			BookOfBible next = BookOfBible.nextBook(book);
			Reference lastVerse = new Reference(next, 1, 1);
			return getReferencesExclusive(firstVerse, lastVerse);
		} else 
			return new ArrayList<Reference>();
	}

	// Override
	public ArrayList<Reference> getReferencesForChapter(BookOfBible book, int chapter) {
		Reference ref1 = new Reference(book, chapter, 1);
		Reference ref2 = new Reference(book, chapter + 1, 1);
		return getReferencesExclusive(ref1, ref2);
	}

	// Override
	public ArrayList<Reference> getReferencesForChapters(BookOfBible book, int chapter1, int chapter2) {
		Reference ref1 = new Reference(book, chapter1, 1);
		Reference ref2 = new Reference(book, chapter2 + 1, 1);
		return getReferencesExclusive(ref1, ref2);
	}

	// Override
	public ArrayList<Reference> getReferencesForPassage(BookOfBible book, int chapter, int verse1, int verse2) {
		Reference ref1 = new Reference(book, chapter, verse1);
		Reference ref2 = new Reference(book, chapter, verse2);
		return getReferencesInclusive(ref1, ref2);
	}

	// Override
	public ArrayList<Reference> getReferencesForPassage(BookOfBible book, int chapter1, int verse1, int chapter2,
			int verse2) {Reference ref1 = new Reference(book, chapter1, verse1);
			Reference ref2 = new Reference(book, chapter2, verse2);
			return getReferencesInclusive(ref1, ref2);
	}

	// Override
	public VerseList getVersesInclusive(Reference firstVerse, Reference lastVerse) {
				VerseList someVerses = new VerseList(getVersion(), firstVerse + "-" + lastVerse);
				if (firstVerse.compareTo(lastVerse) > 0) {
					return someVerses;
				}
				SortedMap<Reference, String> s = theVerses.subMap(firstVerse,true, lastVerse,true);

	
				Set<Map.Entry<Reference, String>> mySet = s.entrySet();


				for (Map.Entry<Reference, String> element : mySet) {
					Verse aVerse = new Verse(element.getKey(), element.getValue());
					someVerses.add(aVerse);
				}
				return someVerses;
			}

	// Override
	public VerseList getVersesExclusive(Reference firstVerse, Reference lastVerse) {
		// Implementation of this method provided by Chuck Cusack.
		// This is provided so you have an example to help you get started
		// with the other methods.

		// We will store the resulting verses here. We copy the version from
		// this Bible and set the description to be the passage that was searched for.
		VerseList someVerses = new VerseList(getVersion(), firstVerse + "-" + lastVerse);

		// Make sure the references are in the correct order. If not, return an empty
		// list.
		if (firstVerse.compareTo(lastVerse) > 0) {
			return someVerses;
		}
		// Return the portion of the TreeMap that contains the verses between
		// the first and the last, not including the last.
		SortedMap<Reference, String> s = theVerses.subMap(firstVerse, lastVerse);

		// Get the entries from the map so we can iterate through them.
		Set<Map.Entry<Reference, String>> mySet = s.entrySet();

		// Iterate through the set and put the verses in the VerseList.
		for (Map.Entry<Reference, String> element : mySet) {
			Verse aVerse = new Verse(element.getKey(), element.getValue());
			someVerses.add(aVerse);
		}
		return someVerses;
	}

	// Override
	public VerseList getBook(BookOfBible book) {
		if (book != null) {
			Reference firstVerse = new Reference(book, 1, 1);
			BookOfBible next = BookOfBible.nextBook(book);
			Reference lastVerse = new Reference(next, 1, 1);
			return getVersesExclusive(firstVerse, lastVerse);
		} else {
			return new VerseList(getVersion(), "invalid book");
		}
	}

	// Override
	public VerseList getChapter(BookOfBible book, int chapter) {
		Reference ref1 = new Reference(book, chapter, 1);
		Reference ref2 = new Reference(book, chapter + 1, 1);
		return getVersesExclusive(ref1, ref2);
	}

	// Override
	public VerseList getChapters(BookOfBible book, int chapter1, int chapter2) {
		Reference ref1 = new Reference(book, chapter1, 1);
		Reference ref2 = new Reference(book, chapter2 + 1, 1);
		return getVersesExclusive(ref1, ref2);
	}

	// Override
	public VerseList getPassage(BookOfBible book, int chapter, int verse1, int verse2) {
				Reference ref1 = new Reference(book, chapter, verse1);
				Reference ref2 = new Reference(book, chapter, verse2);
				return getVersesInclusive(ref1, ref2);
	}

	// Override
	public VerseList getPassage(BookOfBible book, int chapter1, int verse1, int chapter2, int verse2) {
				Reference ref1 = new Reference(book, chapter1, verse1);
				Reference ref2 = new Reference(book, chapter2, verse2);
				return getVersesInclusive(ref1, ref2);
	}

}
