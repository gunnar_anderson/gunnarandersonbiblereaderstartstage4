package bibleReader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import bibleReader.model.Bible;
import bibleReader.model.BookOfBible;
import bibleReader.model.Verse;
import bibleReader.model.VerseList;

/**
 * A utility class that has useful methods to read/write Bibles and Verses.
 * 
 * @author cusack
 * @editedBy Gunnar Anderson
 */
public class BibleIO {

	/**
	 * Read in a file and create a Bible object from it and return it.
	 * 
	 * @param bibleFile
	 * @return
	 */
	// This method is complete, but it won't work until the methods it uses are
	// implemented.
	public static VerseList readBible(File bibleFile) { // Get the extension of
														// the file
		String name = bibleFile.getName();
		String extension = name.substring(name.lastIndexOf('.') + 1, name.length());

		// Call the read method based on the file type.
		if ("atv".equals(extension.toLowerCase())) {
			return readATV(bibleFile);
		} else if ("xmv".equals(extension.toLowerCase())) {
			return readXMV(bibleFile);
		} else {
			return null;
		}
	}

	/**
	 * Read in a Bible that is saved in the "ATV" format. The format is described
	 * below.The first line is a summary of what is in the file. In the case of a
	 * Bible, the first line will be of the following form. ABBREVIATION: FULL TITLE
	 * where ABBREVIATION is generally the acronym used (e.g. KJV for the King James
	 * Version), and FULL TITLE is the full title of the version. Each remaining
	 * line is of the form: BOOK@CHAPTER:VERSE@TEXT For instance, here is Genesis
	 * 1:19 from the kjv file: Ge@1:19@And the evening and the morning were the
	 * fourth day.
	 * 
	 * @param bibleFile The file containing a Bible with .atv extension.
	 * @return A Bible object constructed from the file bibleFile, or null if there
	 *         was an error reading the file.
	 */
	private static VerseList readATV(File bibleFile) {

		VerseList toReturn = new VerseList("string", "string");
		Verse v;
		String version;
		String title;

		try {
			BufferedReader br = new BufferedReader(new FileReader(bibleFile));
			String line1 = br.readLine();
			if (line1.isEmpty()) {
				version = "unknown";
				title = "";
			} else {
				if (line1.contains(": ")) {
					String[] firstln = line1.split(": ", 2);

					version = firstln[0];
					title = firstln[1];
				} else {

					version = line1;
					title = "";
				}

			}
			ArrayList<Verse> catalog = new ArrayList<Verse>();

			String line;
			while ((line = br.readLine()) != null) {
				if (line.contains("@")) {
					String[] temp = line.split("@", 2);
					if (temp[1].contains(":")) {
						String[] temp2 = temp[1].split(":", 2);
						if (temp2[1].contains("@")) {
							String[] temp3 = temp2[1].split("@", 2);

							String bookAbbreviation = temp[0];
							BookOfBible bookBible = BookOfBible.getBookOfBible(bookAbbreviation);
							if (bookBible == null) {
								br.close();
								return null;
							}
							int chapter = Integer.parseInt(temp2[0]);
							int vers = Integer.parseInt(temp3[0]);
							String text = temp3[1];

							v = new Verse(bookBible, chapter, vers, text);
							catalog.add(v);

						}
					}
				} else {
					br.close();
					return null;
				}
			}
			toReturn = new VerseList(version, title, catalog);

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		return toReturn;

	}

	/**
	 * Read in the Bible that is stored in the XMV format.
	 * 
	 * @param bibleFile
	 *            The file containing a Bible with .xmv extension.
	 * @return A Bible object constructed from the file bibleFile, or null if
	 *         there was an error reading the file.
	 */
	private static VerseList readXMV(File bibleFile) {
		// TODO Implement me: Stage 8

		// The XMV is sort of XML-like, but it doesn't have end tags.
		// No description of the file format is given here.
		// You need to look at the file to determine how it should be parsed.

		// TODO Documentation: Stage 8 (Update the Javadoc comment to describe
		// the format of the file.)
		VerseList returnList = new VerseList("string", "string");
		Verse v;
		String version;
		String title;

		try {
			BufferedReader br = new BufferedReader(new FileReader(bibleFile));
			String line1 = br.readLine();
			if (line1.isEmpty()) {
				version = "unknown";
				title = "";
			} else {
				if (line1.contains(": ")) {
					String[] firstln = line1.split(": ", 2);

					version = firstln[0];
					title = firstln[1];
				} else {

					version = line1;
					title = "";
				}

			}
			ArrayList<Verse> catalog = new ArrayList<Verse>();

			String line;
			while ((line = br.readLine()) != null) {
				if (line.contains("@")) {
					String[] temp = line.split("@", 2);
					if (temp[1].contains(":")) {
						String[] temp2 = temp[1].split(":", 2);
						if (temp2[1].contains("@")) {
							String[] temp3 = temp2[1].split("@", 2);

							String bookAbbreviation = temp[0];
							BookOfBible bookBible = BookOfBible.getBookOfBible(bookAbbreviation);
							if (bookBible == null) {
								br.close();
								return null;
							}
							int chapter = Integer.parseInt(temp2[0]);
							int vers = Integer.parseInt(temp3[0]);
							String text = temp3[1];

							v = new Verse(bookBible, chapter, vers, text);
							catalog.add(v);

						}
					}
				} else {
					br.close();
					return null;
				}
			}
			returnList = new VerseList(version, title, catalog);

		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		return returnList;

	}
	

	// Note: In the following methods, we should really ensure that the file
	// extension is correct
	// (i.e. it should be ".atv"). However for now we won't worry about it.
	// Hopefully the GUI code
	// will be written in such a way that it will require the extension to be
	// correct if we are
	// concerned about it.

	/**
	 * Write out the Bible in the ATV format.
	 * 
	 * @param file  The file that the Bible should be written to.
	 * @param bible The Bible that will be written to the file.
	 */
	public static void writeBibleATV(File file, Bible bible) {
		// TODO Implement me: Stage 8
		// Don't forget to write the first line of the file.
		// HINT: This and the next method are very similar. It seems like you
		// might be
		// able to implement one of them and then call it from the other one.
	}

	/**
	 * Write out the given verses in the ATV format, using the description as the
	 * first line of the file.
	 * 
	 * @param file        The file that the Bible should be written to.
	 * @param description The contents that will be placed on the first line of the
	 *                    file, formatted appropriately.
	 * @param verses      The verses that will be written to the file.
	 */
	public static void writeVersesATV(File file, String description, VerseList verses) {
		// TODO Implement me: Stage 8
	}

	/**
	 * Write the string out to the given file. It is presumed that the string is an
	 * HTML rendering of some verses, but really it can be anything.
	 * 
	 * @param file
	 * @param text
	 */
	public static void writeText(File file, String text) {
		// TODO Implement me: Stage 8
		// This one should be really simple.
		// My version is 4 lines of code (not counting the try/catch code).
	}
}
